{ pkgs, ... }: {
  nodes = {
    foo = { pkgs, ... }:
      {
        services.oar.package.default = pkgs.oar2;
        environment.systemPackages = with pkgs; [ oar2 ];
      };
  };
  testScript = ''
    foo.succeed("true")
  '';
}
