{
  description = "nixos-compose - basic setup with external NUR repo";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-21.05";
    kapack.url = "github:oar-team/nur-kapack?ref=nur_overlay";
    # kapack.url = "/home/quentin/dev/nur-kapack";
    nxc.url = "git+https://gitlab.inria.fr/nixos-compose/nixos-compose.git";
  };

  outputs = { self, nixpkgs, kapack, nxc }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};

      extraConfigurations = [
        kapack.nixosModules.oar
      ];

    overlays = [ kapack.overlays.example_nxc ];

    in {
      packages.${system} = nxc.lib.compose {
        inherit nixpkgs system extraConfigurations overlays;
        composition = ./composition.nix;
      };

      defaultPackage.${system} =
        self.packages.${system}."composition::nixos-test";

      devShell.${system} =
        pkgs.mkShell { buildInputs = [ nxc.defaultPackage.${system} ]; };
    };
}
